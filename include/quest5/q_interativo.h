#ifndef Q_INTERATIVO_H
#define Q_INTERATIVO_H

#include <iostream>

using std::cout;
using std::endl;
using std::cin;

void quadrado_i(int n);

#endif