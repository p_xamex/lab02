/**
* @file 	main3.cpp
* @brief 	Programa que, dado um valor A e B, 
*			exibe o MDC dos mesmos
* @author	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	16/03/2017
* @date		19/03/2017
*/

#include <iostream>
#include "mdc.h"

using std::cin;
using std::cout;
using std::endl;
	
int isInt(char v) {
	if ((v-48 < 0 || v-48 > 9)) return 0;
	return 1;
}

void erro() {
	cout << "Entrada inválida!" << endl;
}

int isCorrect(std::string line, int& a, int& b) {
	int space = 0;
	char* input = NULL;
	unsigned i, j;
	for (i = 0; i < line.length(); i++) {
		if (line.at(i) == char(32)) {
			if (space > 1) return 0;
			input = new char[i+1];
			for (j = 0; j < i; j++) {
				input[j] = line.at(j);
			}
			space++;	
		} else if (!isInt(line.at(i))) return 0;
	}

	input[i] = char(0);
	if (!(a = atoi(input))) return 0;
	delete[] input;

	input = new char[line.length()-j+1];
	int m = 0;
	
	for (i = j+1; i < line.length(); i++) {
		input[m] = line.at(i);
		m++;
	}
	
	input[i] = char(0);
	if (!(b = atoi(input))) return 0;
	delete[] input;
	
	return 1;
}

int main() {
	int a, b;
	cout << "Digite dois números naturais positivos: ";
	std::string line;
	std::getline(std::cin, line);
	if (isCorrect(line, a, b)) {
		cout << "MDC(" << a << ", " << b << ") = " << mdc(a,b) << endl;
	} else {
		erro();
	}
}