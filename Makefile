sequencia = ./bin/sequencia

mdc = ./bin/mdc

dec2bin = ./bin/dec2bin

INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++

RM = rm -rf

OBJS_Q01 = ./build/questao01/main1.o ./build/questao01/sequencia.o

OBJS_Q02 = ./build/questao02/main2.o ./build/questao02/mdc.o

OBJS_Q03 = ./build/questao03/main3.o ./build/questao03/dec2binConv.o

CPPFLAGS = -Wall -pedantic -ansi -std=c++11

INC_01 = -I. -I$(INC_DIR)/questao01

INC_02 = -I. -I$(INC_DIR)/questao02

INC_03 = -I. -I$(INC_DIR)/questao03

INC_04 = -I. -I$(INC_DIR)/questao04

INC_05 = -I. -I$(INC_DIR)/questao05

INC_06 = -I. -I$(INC_DIR)/questao06

.PHONY: go clean questao01 dir_01 questao02 all

all: questao01 questao02 questao03	

questao01: $(sequencia) 

$(sequencia): $(OBJS_Q01)
	$(CC) $(OBJS_Q01) $(CPPFLAGS) $(INC_01) -o $@

$(OBJ_DIR)/questao01/main1.o: $(SRC_DIR)/questao01/main1.cpp
	$(CC) -c $(CPPFLAGS) $(INC_01) $^ -o $@

$(OBJ_DIR)/questao01/sequencia.o: $(SRC_DIR)/questao01/sequencia.cpp $(INC_DIR)/questao01/sequencia.h
	$(CC) -c $(CPPFLAGS) $(INC_01) $< -o $@

questao02: $(mdc)

$(mdc):	$(OBJS_Q02)
	$(CC) $^ $(CPPFLAGS) $(INC_02) -o $@

$(OBJ_DIR)/questao02/main2.o: $(SRC_DIR)/questao02/main2.cpp
	$(CC) -c $(CPPFLAGS) $(INC_02) $^ -o $@

$(OBJ_DIR)/questao02/mdc.o: $(SRC_DIR)/questao02/mdc.cpp $(INC_DIR)/questao02/mdc.h
	$(CC) -c $(CPPFLAGS) $(INC_02) $< -o $@

questao03: $(dec2bin)

$(dec2bin): $(OBJS_Q03)
	$(CC) $(OBJS_Q03) $(CPPFLAGS) $(INC_03) -o $@

$(OBJ_DIR)/questao03/main3.o: $(SRC_DIR)/questao03/main3.cpp
	$(CC) -c $(CPPFLAGS) $(INC_03) $^ -o $@		

$(OBJ_DIR)/questao03/dec2binConv.o: $(SRC_DIR)/questao03/dec2binConv.cpp $(INC_DIR)/questao03/dec2binConv.h
	$(CC) -c $(CPPFLAGS) $(INC_03) $< -o $@		
dir:
	mkdir $(BIN_DIR)
	mkdir $(OBJ_DIR)
	mkdir $(OBJ_DIR)/questao01 $(OBJ_DIR)/questao02 $(OBJ_DIR)/questao03 $(OBJ_DIR)/questao04 $(OBJ_DIR)/questao05 $(OBJ_DIR)/questao06

doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile

clean:
	$(RM) $(OBJ_DIR)/questao01/* $(OBJ_DIR)/questao02/*
	$(RM) $(BIN_DIR)/*

