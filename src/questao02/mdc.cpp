#include <cmath>
#include "mdc.h"

int mdc(int a, int b) {
	if (a % b == 0) return b;
	return mdc(a, a % b);
}