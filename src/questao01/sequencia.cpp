#include <cmath>
#include "sequencia.h"

/**
* @brief 	Função que retorna o valor da sequencia recursiva A
* 			para n
* @param 	n N valor utilizado para cálculo da sequencia
* @return 	n = 1/n-1
*/
float sequenciaAR(float n) {
	if (n == 0) return n;
	return 1/n + sequenciaAR(n-1);
}

/**
* @brief 	Função que retorna o valor da sequencia iterativa A
* 			para n
* @param 	n N valor utilizado para cálculo da sequencia
* @return 	p
*/
float sequenciaAI(float n) {
	float p;
	for (; n > 0; n--) {
		p += 1/n;
	}
	return p;
}

/**
* @brief 	Função que retorna o valor da sequencia retursiva B
* 			para n
* @param 	n N valor utilizado para cálculo da sequencia
* @return 	n = n² + 1 / n+3 + n-1
*/
float sequenciaBR(float n) {
	if (n == 0) return n;
	return ((pow(n,2) + 1) / (n+3)) + sequenciaBR(n-1);
}

/**
* @brief 	Função que retorna o valor da sequencia iterativa B
* 			para n
* @param 	n N valor utilizado para cálculo da sequencia
* @return 	p
*/
float sequenciaBI(float n) { 
	float p;
	for (; n > 0; n--) {
		p += (pow(n,2) + 1) / (n+3);
	}
	return p;
}