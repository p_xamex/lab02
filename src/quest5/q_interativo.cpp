#include "q_interativo.h"

void quadrado_i(int n){
	for (int i = 1; i <= n*2; i+=2){
		cout << i;
		if(i+1 < n*2){cout << " + ";}
	}
	cout << " = " << n * n;
}