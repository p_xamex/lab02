#include <iostream>
#include "dec2binConv.h"

using std::cout;

void dec2binConv(int n) {
	if (n == 0) return;
	dec2binConv(n/2);
	cout << n % 2;
}