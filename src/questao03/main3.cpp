/**
* @file 	main3.cpp
* @brief 	Programa que converte um dado número
*			decimal para sua respectiva representação
*			binária
* @author	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	16/03/2017
* @date		19/03/2017
*/

#include <iostream>
#include "dec2binConv.h"

using std::cin;
using std::cout;
using std::endl;

int isInt(std::string& v) {
	int i;
	for (i = 0; v[i] != '\0'; i++) {
		if ((v[i]-48 < 0 || v[i]-48 > 9)) return 0;
	}
	return 1;
}

int getInt(std::string& v) {
	char* integer = new char[v.length()+1];
	unsigned int i;
	for (i = 0; i < v.length(); i++) {
		integer[i] = v.at(i);
	}
	integer[i] = char(0);
	return atoi(integer);
}

void erro() {
	cout << "Entrada inválida!" << endl;
}

int main() {
	std::string input;
	cout << "Digite um número: ";
	std::getline(cin, input);
	if (isInt(input)) {
		cout << "Representação de " << input << " na forma binária: ";
		int n = getInt(input);
		dec2binConv(n);
		cout << endl;
	} else {
		erro();
		return 1;
	}
}