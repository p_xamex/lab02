/**
* @file 	main3.cpp
* @brief 	Programa que exibe o resultado
*			de uma equação recursiva através
*			de um valor inserido pelo usuário
* @details	O usuário terá a opção de escolher
*			se o programa deverá resolver a 
*			equação de forma iterativa ou
*			recursiva
* @author	Ariel Oliveira (ariel.oliveira01@gmail.com)
* @since	16/03/2017
* @date		19/03/2017
*/

#include <iostream>
#include "sequencia.h"

using std::cin;
using std::cout;
using std::endl;
/**
* @brief 	Imprime mensagem de erro na tela
* @return 	void
*/
void erro() {
	cout << "Parametros inválidos!" << endl;
	cout << "Uso: ./bin/sequencia <A ou B> <R ou I> <valor>" << endl;
}

/**
* @brief 	Função que verifica se o vetor de char* v possui apenas 1 caracter,
*		 	depois o compara com o char expected. Caso diferente, retorna 0
* @param 	v V vetor de char que possuirá a entrada do usuário
* @param 	expected EXPECTED caracter esperado para execução do programa
* @return 	booleano
*/
int isTrue(char* v, char expected) {
	int i;
	for (i = 0; v[i] != '\0'; i++) {
		if (i > 1) return 0;
	}
	if (v[0] != expected) return 0;
	return 1;
}

/**
* @brief 	Função que verifica se todos os caracteres no vetor
*			de char* v correspondem a números inteiros
* @param 	v V vetor de char que possuirá a entrada do usuário
* @return 	booleano
*/
int isInt(char* v) {
	int i;
	for (i = 0; v[i] != '\0'; i++) {
		if ((v[i]-48 < 0 || v[i]-48 > 9)) return 0;
	}
	return 1;
}

/**
* @brief 	Procedimento que verifica as entradas dos usuários
*			e faz as chamadas das funções correspondentes
* @param 	v V vetor de char que possuirá a entrada do usuário
* @return 	void
*/
void sequencia(char a, char b, float n) {
	if (a == 'A') {
		if (b == 'R') cout << "O valor da sequência A para N = " << n << " é " << sequenciaAR(n) << " (a versão recursiva foi usada)" << endl;
		else cout << "O valor da sequência A para N = " << n << " é " << sequenciaAR(n) << "(a versão iterativa foi usada)" << endl;
	}
	if (a == 'B') {
		if (b == 'R') cout << "O valor da sequência B para N = " << n << " é " << sequenciaAR(n) << " (a versão recursiva foi usada)" << endl;
		else cout << "O valor da sequência B para N = " << n << " é " << sequenciaAR(n) << " (a versão iterativa foi usada)" << endl;
	}
}

int main(int argc, char* argv[]) {
	if (argc > 3) {
		float n;
		if (!(isTrue(argv[1], 'A') || isTrue(argv[1], 'B'))) {
		 	erro();
		 	return 1;
		}
		if (!(isTrue(argv[2], 'R') || isTrue(argv[2], 'I'))) {
			erro();
			return 1;
		}
		if (isInt(argv[3])) {
			n = atoi(argv[3]);
		} else {
			erro();
			return 1;
		}
		sequencia(argv[1][0], argv[2][0], n);
	} else {
		erro();
		return 1;
	}
	return 0;
}